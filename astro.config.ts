import { defineConfig } from "astro/config";

import node from "@astrojs/node";
import svelte from "@astrojs/svelte";

export default defineConfig({
	output: "server",
	integrations: [svelte()],
	adapter: node({
		mode: "standalone",
	}),
});
