# `astro-serializes-undefined-as-null`

_This bug has been fixed._

A demonstration of `undefined` becoming `null` when a Svelte component whose props are provided by Astro is hydrated.

[Here is the relevant issue.](https://github.com/withastro/astro/issues/7528)

## Testing

### Running a development server

```sh
$ pnpm dev
```

### Running a production server

```sh
$ pnpm build
$ pnpm preview
```
